from django.shortcuts import *
from django.http import *
from django.urls import reverse
from django.views.generic.base import *
from django.views import View
import datetime
from django.db.models import Count, Q, Max
from django.utils import timezone
from .models import *

PATH = 'advertiser_management/template/advertiser_management/'


# Create your views here.
class ShowAds(TemplateView):
    template_name = PATH + 'ads.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        view = View(userIp=self.request.META['REMOTE_ADDR'], viewTime=timezone.now())
        view.save()
        for advertiser in Advertiser.objects.all():
            for ad in advertiser.ad_set.all():
                if ad.approved:
                    view.ads.add(ad)
        view.save()
        context['advertisers'] = Advertiser.objects.all
        return context


class IncreaseClicks(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        ad = get_object_or_404(Ad, id=kwargs['ad_id'])
        click = Click(ad=ad, userIp=self.request.META['REMOTE_ADDR'], clickTime=timezone.now())
        click.save()
        return ad.link


class ShowForm(TemplateView):
    template_name = PATH + "newAd.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['advertisers'] = Advertiser.objects.all
        return context


class SubmitAd(RedirectView):
    pattern_name = 'advertiser_management:showAds'

    def get_redirect_url(self, *args, **kwargs):
        advertiserTmp = Advertiser.objects.get(id=self.request.POST['advertiser'])
        ad = Ad(title=self.request.POST['Title'], link=self.request.POST['Link'], imgUrl=self.request.POST['ImgUrl'],
                advertiser=advertiserTmp)
        ad.save()
        return super().get_redirect_url(*args, **kwargs)


class Report(TemplateView):
    template_name = PATH + 'report.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


def reportClicksAndViews(request):
    res = list(Ad.objects.values('id', 'title'))
    for i in range(0, len(res)):
        ad = Ad.objects.all()[i]
        clickPerHour = [0] * 24
        viewPerHour = [0] * 24
        for j in range(0, 24):
            clickPerHour[j] = ad.click_set.filter(clickTime__hour=j).count()
            viewPerHour[j] = ad.view_set.filter(viewTime__hour=j).count()
        res[i]['clickPerHour'] = clickPerHour
        res[i]['viewPerHour'] = viewPerHour
    return JsonResponse(res, safe=False)


def reportRatio(request):
    ads = Ad.objects.all().annotate(Max('click')).order_by('-click__max')
    res = list(ads.values('id', 'title'))
    for i in range(0, len(res)):
        ad = ads[i]
        ratio = [0] * 24
        allClicks = allViews = 0
        for j in range(0 , 24):
            clicks = ad.click_set.filter(clickTime__hour=j).count()
            views = ad.view_set.filter(viewTime__hour=j).count()
            allClicks += clicks
            allViews += views
            if views != 0:
                ratio[j] = clicks / views
            else:
                ratio[j] = 'N/A'
        if allViews != 0:
            res[i]['allTimeRatio'] = allClicks / allViews
        else:
            res[i]['allTimeRatio'] = 'N/A'
        res[i]['ratio'] = ratio
    return JsonResponse(res, safe=False)


def dateTimeFieldToInt(date):
    return ((((date.year * 365 + date.day) * 24 + date.hour) * 60) + date.minute * 60) + date.second


def reportTimeToClick(request):
    res = list(Ad.objects.all().values('id', 'title'))
    clicks = list(Click.objects.all().order_by('clickTime').values('clickTime'))
    for i in range(0, len(res)):
        ad = Ad.objects.all()[i]
        clickSet = ad.click_set.all().order_by('clickTime')
        viewSet = ad.view_set.all().order_by('viewTime')
        ptV = 0
        ptC = 0
        avg = 0
        for click in clickSet:
            while ptV + 1 < viewSet.count() and viewSet[ptV + 1].viewTime <= click.clickTime:
                ptV += 1
            while ptC + 1 < len(clicks) and clicks[ptC + 1]['clickTime'] < click.clickTime:
                ptC += 1
            tmp1 = viewSet[ptV].viewTime
            tmp2 = clicks[ptC]['clickTime']
            if click.clickTime > tmp2 > tmp1:
                tmp1 = tmp2
            avg += dateTimeFieldToInt(click.clickTime) - dateTimeFieldToInt(tmp1)
        if clickSet.count():
            avg /= clickSet.count()
        else:
            avg = 'N/A'
        res[i]['average'] = avg
    return JsonResponse(res, safe=False)
