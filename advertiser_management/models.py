from django.db import models


# Create your models here.

class Advertiser(models.Model):
    name = models.CharField(max_length=200)

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def describeMe(self):
        return "I'm an advertiser. My name is " + self.name + "."

    def __str__(self):
        return self.name

    def help():
        s = "setName(name) to set the name.\n"
        s += "getName() to get the name.\n"
        s += "getClicks() and getViews() to get the clicks or views.\n"
        s += "incClicks() and incViews() to increase clicks or views by 1.\n"
        s += "describeMe() for description.\n"
        s += "getTotalClicks() for getting the total number of clicks.\n"
        s += "and of course, help() to get help."
        return s


class Ad(models.Model):
    title = models.CharField(max_length=200)
    imgUrl = models.URLField(max_length=200)
    link = models.URLField(max_length=200)
    advertiser = models.ForeignKey(Advertiser, on_delete=models.CASCADE)
    approved = models.BooleanField(default=0)

    def getTitle(self):
        return self.title

    def setTitle(self, title):
        self.title = title

    def getImgUrl(self):
        return self.imgUrl

    def setImgUrl(self, imgUrl):
        self.imgUrl = imgUrl

    def getLink(self):
        return self.link

    def setLink(self, link):
        self.link = link

    def setAdvertiser(self, advertiser):
        self.advertiser = advertiser

    def describeMe(self):
        s = "I'm an ad and my title is " + self.title + ".\n"
        s += "My advertiser is " + self.advertiser.getName() + "."
        return s

    def __str__(self):
        return self.title


class Click(models.Model):
    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
    clickTime = models.DateTimeField('time clicked')
    userIp = models.GenericIPAddressField()

    def __str__(self):
        return self.userIp


class View(models.Model):
    ads = models.ManyToManyField(Ad)
    viewTime = models.DateTimeField('time viewed')
    userIp = models.GenericIPAddressField()

    def __str__(self):
        return self.userIp


