from django.contrib import admin

# Register your models here.
from .models import *


def approve(modeladmin, request, queryset):
    queryset.update(approved=True)


def disapprove(modeladmin, request, queryset):
    queryset.update(approved=False)


approve.short_description = "Approve all selected ads"
disapprove.short_description = "Disapprove all selected ads"


class AdAdmin(admin.ModelAdmin):
    list_display = ('title', 'approved')
    list_filter = ('approved',)
    search_fields = ('title',)
    actions = [approve, disapprove]


admin.site.register(Ad, AdAdmin)
