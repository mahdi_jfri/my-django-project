from django.urls import path
from . import views
from .views import *

app_name = 'advertiser_management'
urlpatterns = [
    path('', ShowAds.as_view(), name='showAds'),
    path('click/<int:ad_id>/', IncreaseClicks.as_view(), name='increaseClicks'),
    path('createad/', ShowForm.as_view(), name='showForm'),
    path('submitad/', SubmitAd.as_view(), name='submitAd'),
    path('report/', Report.as_view(), name='report'),
    path('report/clicks_and_views/', views.reportClicksAndViews, name='reportClicksAndViews'),
    path('report/ratio/', views.reportRatio, name='reportRatio'),
    path('report/time_to_click/', views.reportTimeToClick, name='reportTimeToClick')
]
